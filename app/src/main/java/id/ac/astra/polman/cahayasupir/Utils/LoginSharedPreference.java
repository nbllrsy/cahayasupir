package id.ac.astra.polman.cahayasupir.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;

import id.ac.astra.polman.cahayasupir.Model.Supir;

public class LoginSharedPreference {
    private String KEY_SUPIR = "supir";
    private String KEY_HASLOGIN = "haslogin";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Gson gson;

    public LoginSharedPreference(Context context) {
        String PREF_NAME = "loginPref";
        sharedPreferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        gson = new Gson();
    }

    public Supir getSupir() {
        String jsonToModel = sharedPreferences.getString(KEY_SUPIR,null);
        if(jsonToModel != null) return gson.fromJson(jsonToModel,Supir.class);
        else return null;
    }
    public void setSupir(Supir supir) {
        if(supir != null) {
            String json = gson.toJson(supir);
            editor.putString(KEY_SUPIR, json);
            editor.apply();
        }
    }

    public boolean getHasLogin() { return sharedPreferences.getBoolean(KEY_HASLOGIN, false); }
    public void setHasLogin(boolean hasLogin) {
        editor.putBoolean(KEY_HASLOGIN,hasLogin);
        editor.apply();
    }


}
