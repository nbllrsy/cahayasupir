package id.ac.astra.polman.cahayasupir.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Transaksi implements Parcelable {
    private String id_transaksi;
    private String id_admin;
    private String nama_supir;
    private String plat_no;
    private String nama_tallyman;
    private Date tanggal;
    private Date jam;
    private int status;
    private String quary;
    private String no_kontainer;
    private String no_seal;
    private String ukuran_kontainer;
    private String tipe_kontainer;

    private String time_up_empty;
    private String time_in_factory;
    private String no_loading;
    private String time_loading;
    private String stuffing_in;
    private String stuffing_out;
    private String time_out_factory;
    private String ship;
    private int status_order;

    public Transaksi(String id_transaksi, String nama_supir, String plat_no,
                     String nama_tallyman, Date tanggal, Date jam,
                     int status, String quary, String no_kontainer,
                     String no_seal, String ukuran_kontainer,
                     String tipe_kontainer, int status_order) {
        this.id_transaksi = id_transaksi;
        this.nama_supir = nama_supir;
        this.plat_no = plat_no;
        this.nama_tallyman= nama_tallyman;
        this.tanggal = tanggal;
        this.jam = jam;
        this.status = status;
        this.quary = quary;
        this.no_kontainer = no_kontainer;
        this.no_seal = no_seal;
        this.ukuran_kontainer = ukuran_kontainer;
        this.tipe_kontainer = tipe_kontainer;
        this.status_order = status_order;
    }

    public Transaksi(String id_transaksi, String id_admin, String nama_supir, String plat_no, String nama_tallyman, Date tanggal, Date jam, int status, String quary, String no_kontainer, String no_seal, String ukuran_kontainer, String tipe_kontainer, String time_up_empty, String time_in_factory, String no_loading, String time_loading, String stuffing_in, String stuffing_out, String time_out_factory, String ship, int status_order) {
        this.id_transaksi = id_transaksi;
        this.id_admin = id_admin;
        this.nama_supir= nama_supir;
        this.plat_no = plat_no;
        this.nama_tallyman = nama_tallyman;
        this.tanggal = tanggal;
        this.jam = jam;
        this.status = status;
        this.quary = quary;
        this.no_kontainer = no_kontainer;
        this.no_seal = no_seal;
        this.ukuran_kontainer = ukuran_kontainer;
        this.tipe_kontainer = tipe_kontainer;
        this.time_up_empty = time_up_empty;
        this.time_in_factory = time_in_factory;
        this.no_loading = no_loading;
        this.time_loading = time_loading;
        this.stuffing_in = stuffing_in;
        this.stuffing_out = stuffing_out;
        this.time_out_factory = time_out_factory;
        this.ship = ship;
        this.status_order = status_order;
    }

    protected Transaksi(Parcel in) {
        id_transaksi = in.readString();
        id_admin = in.readString();
        nama_supir = in.readString();
        plat_no = in.readString();
        nama_tallyman = in.readString();
        tanggal = (java.util.Date) in.readSerializable();
        jam = (java.util.Date) in.readSerializable();
        status = in.readInt();
        quary = in.readString();
        no_kontainer = in.readString();
        no_seal = in.readString();
        ukuran_kontainer = in.readString();
        tipe_kontainer = in.readString();
        time_up_empty = in.readString();
        time_in_factory = in.readString();
        no_loading = in.readString();
        time_loading = in.readString();
        stuffing_in = in.readString();
        stuffing_out = in.readString();
        time_out_factory = in.readString();
        ship = in.readString();
        status_order = in.readInt();
    }

    public static final Creator<Transaksi> CREATOR = new Creator<Transaksi>() {
        @Override
        public Transaksi createFromParcel(Parcel in) {
            return new Transaksi(in);
        }

        @Override
        public Transaksi[] newArray(int size) {
            return new Transaksi[size];
        }
    };

    public String getId_transaksi() {
        return id_transaksi;
    }

    public void setId_transaksi(String id_transaksi) {
        this.id_transaksi = id_transaksi;
    }

    public String getId_admin() {
        return id_admin;
    }

    public void setId_admin(String id_admin) {
        this.id_admin = id_admin;
    }

    public String getNama_supir() {
        return nama_supir;
    }

    public void setNama_supir(String nama_supir) {
        this.nama_supir = nama_supir;
    }

    public String getPlat_no() {
        return plat_no;
    }

    public void setPlat_no(String plat_no) {
        this.plat_no = plat_no;
    }

    public String getNama_tallyman() {
        return nama_tallyman;
    }

    public void setNama_tallyman(String nama_tallyman) {
        this.nama_tallyman = nama_tallyman;
    }

    public Date getTanggal() {
        return tanggal;
    }

    public void setTanggal(Date tanggal) {
        this.tanggal = tanggal;
    }

    public Date getJam() {
        return jam;
    }

    public void setJam(Date jam) {
        this.jam = jam;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getQuary() {
        return quary;
    }

    public void setQuary(String quary) {
        this.quary = quary;
    }

    public String getNo_kontainer() {
        return no_kontainer;
    }

    public void setNo_kontainer(String no_kontainer) {
        this.no_kontainer = no_kontainer;
    }

    public String getNo_seal() {
        return no_seal;
    }

    public void setNo_seal(String no_seal) {
        this.no_seal = no_seal;
    }

    public String getUkuran_kontainer() {
        return ukuran_kontainer;
    }

    public void setUkuran_kontainer(String ukuran_kontainer) {
        this.ukuran_kontainer = ukuran_kontainer;
    }

    public String getTipe_kontainer() {
        return tipe_kontainer;
    }

    public void setTipe_kontainer(String tipe_kontainer) {
        this.tipe_kontainer = tipe_kontainer;
    }

    public String getTime_up_empty() {
        return time_up_empty;
    }

    public void setTime_up_empty(String time_up_empty) {
        this.time_up_empty = time_up_empty;
    }

    public String getTime_in_factory() {
        return time_in_factory;
    }

    public void setTime_in_factory(String time_in_factory) {
        this.time_in_factory = time_in_factory;
    }

    public String getNo_loading() {
        return no_loading;
    }

    public void setNo_loading(String no_loading) {
        this.no_loading = no_loading;
    }

    public String getTime_loading() {
        return time_loading;
    }

    public void setTime_loading(String time_loading) {
        this.time_loading = time_loading;
    }

    public String getStuffing_in() {
        return stuffing_in;
    }

    public void setStuffing_in(String stuffing_in) {
        this.stuffing_in = stuffing_in;
    }

    public String getStuffing_out() {
        return stuffing_out;
    }

    public void setStuffing_out(String stuffing_out) {
        this.stuffing_out = stuffing_out;
    }

    public String getTime_out_factory() {
        return time_out_factory;
    }

    public void setTime_out_factory(String time_out_factory) {
        this.time_out_factory = time_out_factory;
    }

    public String getShip() {
        return ship;
    }

    public void setShip(String ship) {
        this.ship = ship;
    }

    public int getStatus_order() {
        return status_order;
    }

    public void setStatus_order(int status_order) {
        this.status_order = status_order;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id_transaksi);
        dest.writeString(id_admin);
        dest.writeString(nama_supir);
        dest.writeString(plat_no);
        dest.writeString(nama_tallyman);
        dest.writeSerializable(tanggal);
        dest.writeSerializable(jam);
        dest.writeInt(status);
        dest.writeString(quary);
        dest.writeString(no_kontainer);
        dest.writeString(no_seal);
        dest.writeString(ukuran_kontainer);
        dest.writeString(tipe_kontainer);
        dest.writeString(time_up_empty);
        dest.writeString(time_in_factory);
        dest.writeString(no_loading);
        dest.writeString(time_loading);
        dest.writeString(stuffing_in);
        dest.writeString(stuffing_out);
        dest.writeString(time_out_factory);
        dest.writeString(ship);
        dest.writeInt(status_order);
    }
}
