package id.ac.astra.polman.cahayasupir;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import id.ac.astra.polman.cahayasupir.Fragment.HistoryFragment;
import id.ac.astra.polman.cahayasupir.Fragment.ListFragment;
import id.ac.astra.polman.cahayasupir.Fragment.ProfileFragment;
import id.ac.astra.polman.cahayasupir.Fragment.TaskFragment;


public class MainActivity extends AppCompatActivity {
    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_profile:
                    ProfileFragment profileFragment = new ProfileFragment();
                    ft.replace(R.id.frame_layout, profileFragment, ProfileFragment.class.getSimpleName());
                    ft.commit();
                    return true;
                case R.id.navigation_history:
                    HistoryFragment historyFragment = new HistoryFragment();
                    ft.replace(R.id.frame_layout, historyFragment, HistoryFragment.class.getSimpleName());
                    ft.commit();
                    return true;

                case R.id.navigation_list:
                    TaskFragment taskFragment = new TaskFragment();
                    ft.replace(R.id.frame_layout, taskFragment, TaskFragment.class.getSimpleName());
                    ft.commit();
                    return true;
                case R.id.navigation_task:
                    ListFragment listFragment = new ListFragment();
                    ft.replace(R.id.frame_layout, listFragment, ListFragment.class.getSimpleName());
                    ft.commit();
                    return true;

            }
            return false;
        }
    };

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
            finishAffinity();
        }
        else{
            Toast.makeText(getBaseContext(), "Press once again to exit", Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        mTextMessage = findViewById(R.id.message);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
