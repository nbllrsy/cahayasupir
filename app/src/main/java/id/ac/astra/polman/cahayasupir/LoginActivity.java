package id.ac.astra.polman.cahayasupir;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import id.ac.astra.polman.cahayasupir.Model.Supir;
import id.ac.astra.polman.cahayasupir.Utils.GlobalVars;
import id.ac.astra.polman.cahayasupir.Utils.LoginSharedPreference;

public class LoginActivity extends AppCompatActivity {

    private final static String TAG = LoginActivity.class.getSimpleName();
    private EditText txtUsername, txtPassword;
    private Button btnLogin;
    private LoginSharedPreference loginSharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginSharedPreference = new LoginSharedPreference(this);
        if(loginSharedPreference.getHasLogin()) {
            intentToHome();
        }

        txtPassword = findViewById(R.id.txtPassword);
        txtUsername = findViewById(R.id.txtUsername);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(txtUsername.getText().toString())) {
                    txtUsername.setError("Email harus diisi");
                } else if (TextUtils.isEmpty(txtPassword.getText().toString())) {
                    txtPassword.setError("Kata sandi harus diisi");
                } else {
                    final ProgressDialog progress = new ProgressDialog(LoginActivity.this);
                    progress.setTitle(getString(R.string.information));
                    progress.setMessage(getString(R.string.please_wait));
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.show();

                    try {
                        JSONObject object = new JSONObject();
                        object.put("email", txtUsername.getText().toString());
                        object.put("password", txtPassword.getText().toString());


                        AndroidNetworking
                                .post(GlobalVars.LOGIN_URL)
                                .addJSONObjectBody(object)
                                .setPriority(Priority.MEDIUM)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            if (response.getString("message").equals("success")) {
                                                JSONObject responeObject = response.getJSONObject("data");
                                                Supir supir = new Supir(
                                                        responeObject.getString("nomor_ktp"),
                                                        responeObject.getString("nomor_sim"),
                                                        responeObject.getString("batas_sim"),
                                                        responeObject.getString("nama_supir"),
                                                        responeObject.getString("external_id"),
                                                        responeObject.getString("nomor_kontak"),
                                                        responeObject.getString("email"),
                                                        responeObject.getString("password"),
                                                        responeObject.getString("foto_supir"),
                                                        responeObject.getString("foto_ktp"),
                                                        responeObject.getString("foto_sim"),
                                                        response.getString("token")
                                                );

                                                loginSharedPreference.setSupir(supir);
                                                loginSharedPreference.setHasLogin(true);
                                                intentToHome();
                                            } else {
                                                Toast.makeText(
                                                        LoginActivity.this,
                                                        response.getString("message"),
                                                        Toast.LENGTH_SHORT
                                                ).show();
                                            }
                                        } catch (JSONException e) {
                                            Log.e(TAG, "Error: " + e);
                                            Toast.makeText(LoginActivity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
                                        }
                                        progress.dismiss();
                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Log.e(TAG, "AnError: " + anError.getErrorBody());
                                        Toast.makeText(LoginActivity.this, "AnError: " + anError, Toast.LENGTH_SHORT).show();
                                        progress.dismiss();
                                    }
                                });
                    } catch (JSONException e) {
                        Log.e(TAG, "ProgressLogin: " + e.getMessage());
                        Toast.makeText(LoginActivity.this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        progress.dismiss();
                    }
                }
            }
        });

    }

    private void intentToHome() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            super.onBackPressed();
            finishAffinity();
        }
        else{
            Toast.makeText(getBaseContext(), "Press once again to exit", Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }
}
