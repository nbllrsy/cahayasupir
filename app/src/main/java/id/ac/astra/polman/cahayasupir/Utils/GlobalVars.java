package id.ac.astra.polman.cahayasupir.Utils;

public class GlobalVars {
    public static final String BASE_URL = "https://e8c09637.ngrok.io/Cahaya/index.php/";
    public static final String LOGIN_URL = BASE_URL + "Supir/Login";

    public static final String GETALL_TRANSAKSI_URL = BASE_URL + "Transaksi/getAllTransaksi";
    public static final String GETALL_SUPIR_URL = BASE_URL + "Supir/getListSupir";
    public static final String GETALL_KENDARAAN_URL = BASE_URL + "Kendaraan/getListKendaraan";
    public static final String GETALL_TALLYMAN_URL = BASE_URL + "Tallyman/getListTallyman";
    public static final String GET_DEPO_URL = BASE_URL + "Tempat/getTempatPickUp";

    public static final String GET_UPDATESTAT2_URL = BASE_URL + "Transaksi/changeStat2";
    public static final String GET_UPDATESTAT3_URL = BASE_URL + "Transaksi/changeStat3";
    public static final String GET_TRANSAKSISTAT1_URL = BASE_URL + "Transaksi/getTransaksiStat1";
    public static final String GET_TRANSAKSISTAT2_URL = BASE_URL + "Transaksi/getTransaksiStat2";
    public static final String GET_TRANSAKSISTAT3_URL = BASE_URL + "Transaksi/getTransaksiStat3";
}
