package id.ac.astra.polman.cahayasupir.Fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.Constraints;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import id.ac.astra.polman.cahayasupir.Model.Transaksi;
import id.ac.astra.polman.cahayasupir.R;
import id.ac.astra.polman.cahayasupir.TaskActivity;
import id.ac.astra.polman.cahayasupir.Utils.GlobalVars;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskFragment extends Fragment {
    private CardView mCardViewTask;

    private RecyclerView mTaskRecyclerView;
    private TransaksiAdapter mTransaksiAdapter;

    private List<Transaksi> mTransaksis = new ArrayList<>();

    public TaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {




        View view = inflater.inflate(R.layout.fragment_task, container, false);


        mTaskRecyclerView = (RecyclerView) view.findViewById(R.id.transaksi_recycler_view);
        mTaskRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mTransaksiAdapter = new TransaksiAdapter(mTransaksis);
        mTaskRecyclerView.setAdapter(mTransaksiAdapter);


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getDataFilling();
    }

    public void getDataFilling() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Informasi");
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        AndroidNetworking.post(GlobalVars.GET_TRANSAKSISTAT1_URL)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: " + response);
                        try {
                            JSONArray list = response.getJSONArray("data");

                            for (int i = 0; i < list.length(); i++) {
                                JSONObject data = list.getJSONObject(i);

                                String strTanggal = data.getString("tanggal");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                Date tanggal = sdf.parse(strTanggal);

                                String strJam = data.getString("jam");
                                SimpleDateFormat sdfJam = new SimpleDateFormat("hh:mm:ss");
                                Date jam = sdfJam.parse(strJam);

                                String idtransaksi = data.getString("id_transaksi");
                                String nama_supir = data.getString("nama_supir");
                                String plat_no = data.getString("plat_no");
                                String nama_tallyman = data.getString("nama_tallyman");
//                                String tanggal = data.getString("tanggal");
//                                String jam = data.getString("jam");
                                int status = data.getInt("status");
                                String quary = data.getString("quary");
                                String no_kontainer = data.getString("no_kontainer");
                                String no_seal = data.getString("no_seal");
                                String ukuran_kontainer = data.getString("ukuran_kontainer");
                                String tipe_kontainer = data.getString("tipe_kontainer");
                                int status_order = data.getInt("status_order");

                                mTransaksis.add(new Transaksi(
                                        idtransaksi,
                                        nama_supir,
                                        plat_no,
                                        nama_tallyman,
                                        tanggal,
                                        jam,
                                        status,
                                        quary,
                                        no_kontainer,
                                        no_seal,
                                        ukuran_kontainer,
                                        tipe_kontainer,
                                        status_order
                                ));
                            }
//
//                            if (mTransaksiAdapter == null){
//                                mTransaksiAdapter = new TransaksiAdapter(mTransaksis);
//                                mTaskRecyclerView.setAdapter(mTransaksiAdapter);
//                            } else {
//                                Log.e("size", String.valueOf(mTransaksis.size()));
//                                mTransaksiAdapter.setTransaksis(mTransaksis);
//                                mTransaksiAdapter.notifyDataSetChanged();
//                            }
                            Log.e("size", String.valueOf(mTransaksis.size()));
                            mTransaksiAdapter.setTransaksis(mTransaksis);
                            mTransaksiAdapter.notifyDataSetChanged();
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(Constraints.TAG, "onResponse: " + e.getMessage());
                            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        } catch (ParseException e) {
                            e.printStackTrace();
                            Log.e(Constraints.TAG, "onResponse: " + e.getMessage() );
                            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(Constraints.TAG, "onError: " + anError.getMessage());
                        Toast.makeText(getActivity(), "AnError: " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                });
    }

    private class TransaksiHolder extends RecyclerView.ViewHolder{


        private TextView mTvIdOrder;
        private TextView mTvTanggal;
        private TextView mTvJam;
        private TextView mTvKtp;
        private TextView mTvKendaraan;
        private TextView mTvStatus;

        private TextView mTvStatusOrder;

        private Transaksi mTransaksi;

        public TransaksiHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_task, parent, false));


            mTvIdOrder = itemView.findViewById(R.id.tvIdOrder);
            mTvTanggal = itemView.findViewById(R.id.tvTanggal);
            mTvJam = itemView.findViewById(R.id.tvJam);
            mTvKtp = itemView.findViewById(R.id.tvKtp);
            mTvKendaraan = itemView.findViewById(R.id.tvKendaraan);
            mTvStatus = itemView.findViewById(R.id.tvStatus);
            mCardViewTask = (CardView) itemView.findViewById(R.id.cardview);
            mCardViewTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent profilForm = TaskActivity.newIntent(getActivity(),mTransaksi.getId_transaksi(),
                            mTransaksi.getNama_supir(),
                            mTransaksi.getPlat_no(),
                            mTransaksi.getNama_tallyman(),
                            mTransaksi.getTanggal(),
                            mTransaksi.getJam(),
                            mTransaksi.getStatus(),
                            mTransaksi.getQuary(),
                            mTransaksi.getNo_kontainer(),
                            mTransaksi.getNo_seal(),
                            mTransaksi.getUkuran_kontainer(),
                            mTransaksi.getTipe_kontainer(),
                            mTransaksi.getStatus_order());
                    startActivity(profilForm);
                }
            });

//            mTvNoKontainer = itemView.findViewById(R.id.tvNoKontainer);
//            mTvNoSeal = itemView.findViewById(R.id.tvNoSeal);
//            mTvUkuranKontainer = itemView.findViewById(R.id.tvUkuranKontainer);
//            mTvTipeKontainer = itemView.findViewById(R.id.tvTipeKontainer);
//            mTvQuary = itemView.findViewById(R.id.tvQuary);
            mTvStatusOrder = itemView.findViewById(R.id.tvStatusOrder);
        }

        public void bind(Transaksi transaksi) {

            mTransaksi = transaksi;

            mTvIdOrder.setText(String.valueOf(mTransaksi.getId_transaksi()));

            Locale id = new Locale("id");
//            android.text.format.DateFormat df = new android.text.format.DateFormat();
//            mTvTanggal.setText(df.format("EEEE, dd MMMM yyyy", mTransaksi.getTanggal()));
            mTvTanggal.setText(new SimpleDateFormat("EEEE, dd MMMM yyyy", id).format(mTransaksi.getTanggal()));

//            android.text.format.DateFormat dfJam = new android.text.format.DateFormat();
//            mTvJam.setText(dfJam.format("HH:mm a", mTransaksi.getJam()));
            mTvJam.setText(new SimpleDateFormat("HH:mm a", id).format(mTransaksi.getJam()));

//          mTvTanggal.setText(mTransaksi.getTanggal());
//            mTvJam.setText(mTransaksi.getJam());

            mTvKtp.setText(String.valueOf(mTransaksi.getNama_supir()));
            mTvKendaraan.setText(String.valueOf(mTransaksi.getPlat_no()));

            if (mTransaksi.getStatus() == 1) {
                mTvStatus.setText("Reguler");
            } else {
                mTvStatus.setText("Dini");
            }

//            mTvNoKontainer.setText(String.valueOf(mTransaksi.getNo_kontainer()));
//            mTvNoSeal.setText(String.valueOf(mTransaksi.getNo_seal()));
//            mTvUkuranKontainer.setText(String.valueOf(mTransaksi.getUkuran_kontainer()));
//            mTvTipeKontainer.setText(String.valueOf(mTransaksi.getTipe_kontainer()));
//            mTvQuary.setText(String.valueOf(mTransaksi.getQuary()));

            if (mTransaksi.getStatus_order() == 1) {
                mTvStatusOrder.setText("Diterima Supir");
            } else if (mTransaksi.getStatus_order() == 2){
                mTvStatusOrder.setText("Sedang Dijalankan");
            } else{
                mTvStatusOrder.setText("Selesai Dijalankan");
            }
        }


    }

    private class TransaksiAdapter extends RecyclerView.Adapter<TransaksiHolder> {

        private List<Transaksi> mTransaksis;

        public TransaksiAdapter(List<Transaksi> transaksis) {
            this.mTransaksis = transaksis;
        }

        @Override
        public TransaksiHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());

            return new TransaksiHolder(layoutInflater, viewGroup);
        }

        @Override
        public void onBindViewHolder(TransaksiHolder transaksiHolder, int i) {
            Transaksi transaksi = mTransaksis.get(i);
            transaksiHolder.bind(transaksi);
        }

        @Override
        public int getItemCount() {
            return mTransaksis.size();
        }

        public void setTransaksis(List<Transaksi> transaksis) {
            mTransaksis = transaksis;
        }
    }


}
