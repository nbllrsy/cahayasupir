package id.ac.astra.polman.cahayasupir;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import id.ac.astra.polman.cahayasupir.Utils.LoginSharedPreference;

public class EditProfileActivity extends AppCompatActivity {
    private FloatingActionButton mButtonSave;
    private EditText mTextNama;
    private EditText mTextNomor;
    private EditText mTextEmail;
    private EditText mTextPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);


        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ubah Profil");


        mTextNama = findViewById(R.id.nama_supir);
        mTextNomor = findViewById(R.id.nomor_kontak);
        mTextEmail = findViewById(R.id.email);
        mTextPass = findViewById(R.id.password);
        mButtonSave = findViewById(R.id.save);

        mTextNama.setText(new LoginSharedPreference(getApplicationContext()).getSupir().getNama_supir());
        mTextNomor.setText(new LoginSharedPreference(getApplicationContext()).getSupir().getNomor_kontak());
        mTextEmail.setText(new LoginSharedPreference(getApplicationContext()).getSupir().getEmail());
        mTextPass.setText(new LoginSharedPreference(getApplicationContext()).getSupir().getPassword());



    }
}
