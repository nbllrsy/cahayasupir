package id.ac.astra.polman.cahayasupir.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Supir implements Parcelable {
    private String nomor_ktp;
    private String nomor_sim;
    private String batas_sim;
    private String nama_supir;
    private String external_id;
    private String nomor_kontak;
    private String email;
    private String password;
    private String foto_supir;
    private String foto_ktp;
    private String foto_sim;
    private String auth_token;

    public Supir() {
    }

    public Supir(String nomor_ktp, String nomor_sim, String batas_sim, String nama_supir, String external_id, String nomor_kontak, String email, String password, String foto_supir, String foto_ktp, String foto_sim, String auth_token) {
        this.nomor_ktp = nomor_ktp;
        this.nomor_sim = nomor_sim;
        this.batas_sim = batas_sim;
        this.nama_supir = nama_supir;
        this.external_id = external_id;
        this.nomor_kontak = nomor_kontak;
        this.email = email;
        this.password = password;
        this.foto_supir = foto_supir;
        this.foto_ktp = foto_ktp;
        this.foto_sim = foto_sim;
        this.auth_token = auth_token;
    }

    protected Supir(Parcel in) {
        nomor_ktp = in.readString();
        nomor_sim = in.readString();
        batas_sim = in.readString();
        nama_supir = in.readString();
        external_id = in.readString();
        nomor_kontak = in.readString();
        email = in.readString();
        password = in.readString();
        foto_supir = in.readString();
        foto_ktp = in.readString();
        foto_sim = in.readString();
        auth_token = in.readString();
    }


    public static final Creator<Supir> CREATOR = new Creator<Supir>() {
        @Override
        public Supir createFromParcel(Parcel in) {
            return new Supir(in);
        }

        @Override
        public Supir[] newArray(int size) {
            return new Supir[size];
        }
    };

    public String getNomor_ktp() {
        return nomor_ktp;
    }

    public void setNomor_ktp(String nomor_ktp) {
        this.nomor_ktp = nomor_ktp;
    }

    public String getNomor_sim() {
        return nomor_sim;
    }

    public void setNomor_sim(String nomor_sim) {
        this.nomor_sim = nomor_sim;
    }

    public String getBatas_sim() {
        return batas_sim;
    }

    public void setBatas_sim(String batas_sim) {
        this.batas_sim = batas_sim;
    }

    public String getNama_supir() {
        return nama_supir;
    }

    public void setNama_supir(String nama_supir) {
        this.nama_supir = nama_supir;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getNomor_kontak() {
        return nomor_kontak;
    }

    public void setNomor_kontak(String nomor_kontak) {
        this.nomor_kontak = nomor_kontak;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFoto_supir() {
        return foto_supir;
    }

    public void setFoto_supir(String foto_supir) {
        this.foto_supir = foto_supir;
    }

    public String getFoto_ktp() {
        return foto_ktp;
    }

    public void setFoto_ktp(String foto_ktp) {
        this.foto_ktp = foto_ktp;
    }

    public String getFoto_sim() {
        return foto_sim;
    }

    public void setFoto_sim(String foto_sim) {
        this.foto_sim = foto_sim;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(nomor_ktp);
        dest.writeString(nomor_sim);
        dest.writeString(batas_sim);
        dest.writeString(nama_supir);
        dest.writeString(external_id);
        dest.writeString(nomor_kontak);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(foto_supir);
        dest.writeString(foto_ktp);
        dest.writeString(foto_sim);
        dest.writeString(auth_token);
    }
}
