package id.ac.astra.polman.cahayasupir.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Tallyman implements Parcelable {
    private String id_tallyman;
    private String nama;

    public Tallyman() {
    }

    public Tallyman(String id_tallyman, String nama) {
        this.id_tallyman = id_tallyman;
        this.nama = nama;
    }

    public String getId_tallyman() {
        return id_tallyman;
    }

    public void setId_tallyman(String id_tallyman) {
        this.id_tallyman = id_tallyman;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    protected Tallyman(Parcel in) {
        id_tallyman = in.readString();
        nama = in.readString();
    }

    public static final Creator<Tallyman> CREATOR = new Creator<Tallyman>() {
        @Override
        public Tallyman createFromParcel(Parcel in) {
            return new Tallyman(in);
        }

        @Override
        public Tallyman[] newArray(int size) {
            return new Tallyman[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id_tallyman);
        dest.writeString(nama);
    }
}
