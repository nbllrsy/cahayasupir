package id.ac.astra.polman.cahayasupir.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Kendaraan implements Parcelable {
    private String plat_no;
    private String external_id;
    private String nomor_stnk;
    private String nomor_kir_head;
    private String nomor_kir_tail;
    private String batas_kir_head;
    private String batas_kir_tail;
    private String batas_stnk;
    private String foto_kir_head;
    private String foto_kir_tail;
    private String foto_truk;
    private String foto_stnk;

    public String getPlat_no() {
        return plat_no;
    }

    public void setPlat_no(String plat_no) {
        this.plat_no = plat_no;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getNomor_stnk() {
        return nomor_stnk;
    }

    public void setNomor_stnk(String nomor_stnk) {
        this.nomor_stnk = nomor_stnk;
    }

    public String getNomor_kir_head() {
        return nomor_kir_head;
    }

    public void setNomor_kir_head(String nomor_kir_head) {
        this.nomor_kir_head = nomor_kir_head;
    }

    public String getNomor_kir_tail() {
        return nomor_kir_tail;
    }

    public void setNomor_kir_tail(String nomor_kir_tail) {
        this.nomor_kir_tail = nomor_kir_tail;
    }

    public String getBatas_kir_head() {
        return batas_kir_head;
    }

    public void setBatas_kir_head(String batas_kir_head) {
        this.batas_kir_head = batas_kir_head;
    }

    public String getBatas_kir_tail() {
        return batas_kir_tail;
    }

    public void setBatas_kir_tail(String batas_kir_tail) {
        this.batas_kir_tail = batas_kir_tail;
    }

    public String getBatas_stnk() {
        return batas_stnk;
    }

    public void setBatas_stnk(String batas_stnk) {
        this.batas_stnk = batas_stnk;
    }

    public String getFoto_kir_head() {
        return foto_kir_head;
    }

    public void setFoto_kir_head(String foto_kir_head) {
        this.foto_kir_head = foto_kir_head;
    }

    public String getFoto_kir_tail() {
        return foto_kir_tail;
    }

    public void setFoto_kir_tail(String foto_kir_tail) {
        this.foto_kir_tail = foto_kir_tail;
    }

    public String getFoto_truk() {
        return foto_truk;
    }

    public void setFoto_truk(String foto_truk) {
        this.foto_truk = foto_truk;
    }

    public String getFoto_stnk() {
        return foto_stnk;
    }

    public void setFoto_stnk(String foto_stnk) {
        this.foto_stnk = foto_stnk;
    }

    public Kendaraan() {
    }

    public Kendaraan(String plat_no, String external_id, String nomor_stnk, String nomor_kir_head, String nomor_kir_tail, String batas_kir_head, String batas_kir_tail, String batas_stnk, String foto_kir_head, String foto_kir_tail, String foto_truk, String foto_stnk) {
        this.plat_no = plat_no;
        this.external_id = external_id;
        this.nomor_stnk = nomor_stnk;
        this.nomor_kir_head = nomor_kir_head;
        this.nomor_kir_tail = nomor_kir_tail;
        this.batas_kir_head = batas_kir_head;
        this.batas_kir_tail = batas_kir_tail;
        this.batas_stnk = batas_stnk;
        this.foto_kir_head = foto_kir_head;
        this.foto_kir_tail = foto_kir_tail;
        this.foto_truk = foto_truk;
        this.foto_stnk = foto_stnk;
    }

    protected Kendaraan(Parcel in) {
        plat_no = in.readString();
        external_id = in.readString();
        nomor_stnk = in.readString();
        nomor_kir_head = in.readString();
        nomor_kir_tail = in.readString();
        batas_kir_head = in.readString();
        batas_kir_tail = in.readString();
        batas_stnk = in.readString();
        foto_kir_head = in.readString();
        foto_kir_tail = in.readString();
        foto_truk = in.readString();
        foto_stnk = in.readString();
    }

    public static final Creator<Kendaraan> CREATOR = new Creator<Kendaraan>() {
        @Override
        public Kendaraan createFromParcel(Parcel in) {
            return new Kendaraan(in);
        }

        @Override
        public Kendaraan[] newArray(int size) {
            return new Kendaraan[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(plat_no);
        dest.writeString(external_id);
        dest.writeString(nomor_stnk);
        dest.writeString(nomor_kir_head);
        dest.writeString(nomor_kir_tail);
        dest.writeString(batas_kir_head);
        dest.writeString(batas_kir_tail);
        dest.writeString(batas_stnk);
        dest.writeString(foto_kir_head);
        dest.writeString(foto_kir_tail);
        dest.writeString(foto_truk);
        dest.writeString(foto_stnk);
    }
}
