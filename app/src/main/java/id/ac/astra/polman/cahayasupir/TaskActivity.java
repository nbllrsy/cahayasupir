package id.ac.astra.polman.cahayasupir;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import id.ac.astra.polman.cahayasupir.Utils.GlobalVars;

public class TaskActivity extends AppCompatActivity {
    private static final String TAG = TaskActivity.class.getSimpleName();
    private Button mButtonAmbil;
    private EditText mTextID;
    private EditText mTextNama;
    private EditText mTextPlatNo;
    private EditText mTextTallyman;
    private EditText mTextTanggal;
    private EditText mTextJam;
    private EditText mTextStatus;
    private EditText mTextQuarry;
    private EditText mTextKontainer;
    private EditText mTextSeal;
    private EditText mTextUkKontainer;
    private EditText mTextTipeKontainer;
    private EditText mTextStatusOrder;

    private static final String EXTRA_TASK_ID =
            "id.ac.astra.polman.cahaya.id_transaksi";
    private static final String EXTRA_TASK_NAMA =
            "id.ac.astra.polman.cahaya.nama_supir";
    private static final String EXTRA_TASK_PLATNO =
            "id.ac.astra.polman.cahaya.plat_no";
    private static final String EXTRA_TASK_TALLYMAN =
            "id.ac.astra.polman.cahaya.nama_tallyman";
    private static final String EXTRA_TASK_TANGGAL =
            "id.ac.astra.polman.cahaya.tanggal";
    private static final String EXTRA_TASK_JAM =
            "id.ac.astra.polman.cahaya.jam";
    private static final String EXTRA_TASK_STATUS =
            "id.ac.astra.polman.cahaya.status";
    private static final String EXTRA_TASK_QUARRY =
            "id.ac.astra.polman.cahaya.quary";
    private static final String EXTRA_TASK_KONTAINER =
            "id.ac.astra.polman.cahaya.no_kontainer";
    private static final String EXTRA_TASK_SEAL =
            "id.ac.astra.polman.cahaya.no_seal";
    private static final String EXTRA_TASK_UK_KONTAINER =
            "id.ac.astra.polman.cahaya.ukuran_kontainer";
    private static final String EXTRA_TASK_TIPE_KONTAINER =
            "id.ac.astra.polman.cahaya.tipe_kontainer";
    private static final String EXTRA_TASK_STATUS_ORDER =
            "id.ac.astra.polman.cahaya.status_order";

    public static Intent newIntent(Context packageContext, String id_transaksi, String nama_supir, String plat_no,
                                   String nama_tallyman, Date tanggal, Date jam,
                                   int status, String quary, String no_kontainer,
                                   String no_seal, String ukuran_kontainer,
                                   String tipe_kontainer, int status_order) {
        Intent intent = new Intent(packageContext, TaskActivity.class);
        intent.putExtra(EXTRA_TASK_ID, id_transaksi);
        intent.putExtra(EXTRA_TASK_NAMA, nama_supir);
        intent.putExtra(EXTRA_TASK_PLATNO, plat_no);
        intent.putExtra(EXTRA_TASK_TALLYMAN, nama_tallyman);
        intent.putExtra(EXTRA_TASK_TANGGAL, tanggal);
        intent.putExtra(EXTRA_TASK_JAM, jam);
        intent.putExtra(EXTRA_TASK_STATUS, status);
        intent.putExtra(EXTRA_TASK_QUARRY, quary);
        intent.putExtra(EXTRA_TASK_KONTAINER, no_kontainer);
        intent.putExtra(EXTRA_TASK_SEAL, no_seal);
        intent.putExtra(EXTRA_TASK_UK_KONTAINER, ukuran_kontainer);
        intent.putExtra(EXTRA_TASK_TIPE_KONTAINER, tipe_kontainer);
        intent.putExtra(EXTRA_TASK_STATUS_ORDER, status_order);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        String ID = (String) getIntent().getSerializableExtra(EXTRA_TASK_ID);
        String NAMA = (String) getIntent().getSerializableExtra(EXTRA_TASK_NAMA);
        String PLATNO = (String) getIntent().getSerializableExtra(EXTRA_TASK_PLATNO);
        String TALLYMAN = (String) getIntent().getSerializableExtra(EXTRA_TASK_TALLYMAN);
        Date TANGGAL = (Date) getIntent().getSerializableExtra(EXTRA_TASK_TANGGAL);
        Date JAM = (Date) getIntent().getSerializableExtra(EXTRA_TASK_JAM);
        Integer STATUS = (Integer) getIntent().getSerializableExtra(EXTRA_TASK_STATUS);
        String QUARRY = (String) getIntent().getSerializableExtra(EXTRA_TASK_QUARRY);
        String KONTAINER = (String) getIntent().getSerializableExtra(EXTRA_TASK_KONTAINER);
        String SEAL = (String) getIntent().getSerializableExtra(EXTRA_TASK_SEAL);
        String UKKONTAINER = (String) getIntent().getSerializableExtra(EXTRA_TASK_UK_KONTAINER);
        String TIPEKONTAINER = (String) getIntent().getSerializableExtra(EXTRA_TASK_TIPE_KONTAINER);
        Integer STATUSORDER = (Integer) getIntent().getSerializableExtra(EXTRA_TASK_STATUS_ORDER);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Detail Pesanan");

        mTextID = findViewById(R.id.txtIdTr);
        mTextNama = findViewById(R.id.txtNamaSupir);
        mTextPlatNo = findViewById(R.id.txtKendaraan);
        mTextTallyman = findViewById(R.id.txtTallyman);
        mTextTanggal = findViewById(R.id.txtTanggal);
        mTextJam = findViewById(R.id.txtJam);
        mTextStatus = findViewById(R.id.txtStatus);
        mTextQuarry = findViewById(R.id.txtQuarry);
        mTextKontainer = findViewById(R.id.txtNoKon);
        mTextSeal = findViewById(R.id.txtSeal);
        mTextUkKontainer = findViewById(R.id.txtUkuran);
        mTextTipeKontainer = findViewById(R.id.txtType);
        mTextStatusOrder = findViewById(R.id.save);
        mButtonAmbil = (Button) findViewById(R.id.btnAmbil);
        mButtonAmbil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStat();
                Intent intent = new Intent(TaskActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        Date tgl = null;
        Date jam = null;
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        SimpleDateFormat sdfJ = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        try {
            tgl=sdf.parse(String.valueOf(TANGGAL));
            jam=sdfJ.parse(String.valueOf(JAM));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
        SimpleDateFormat sdfJ2 = new SimpleDateFormat("HH:mm a");
        String tgl2 = sdf2.format(tgl);
        String jam2 = sdfJ2.format(jam);


        mTextID.setText(ID);
        mTextNama.setText(NAMA);
        mTextPlatNo.setText(PLATNO);
        mTextTallyman.setText(TALLYMAN);
        mTextTanggal.setText(tgl2);
        mTextJam.setText(jam2);
        mTextStatus.setText(STATUS.toString());
        mTextQuarry.setText(QUARRY);
        mTextKontainer.setText(KONTAINER);
        mTextSeal.setText(SEAL);
        mTextUkKontainer.setText(UKKONTAINER + " FEET");
        mTextTipeKontainer.setText(TIPEKONTAINER);


    }

    private void changeStat(){
        final ProgressDialog progress = new ProgressDialog(TaskActivity.this);
        progress.setTitle(getString(R.string.information));
        progress.setMessage(getString(R.string.please_wait));
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.show();


        AndroidNetworking.post(GlobalVars.GET_UPDATESTAT2_URL)
                .addBodyParameter("id_transaksi", mTextID.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("message").equals("success")) {
                                JSONObject responeObject = response.getJSONObject("data");

                            } else {
                                Toast.makeText(
                                        TaskActivity.this,
                                        response.getString("message"),
                                        Toast.LENGTH_SHORT
                                ).show();
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "Data Jadwal Tidak Ada!" );
                            Toast.makeText(TaskActivity.this, "Data Jadwal Tidak Ada!", Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "AnError: " + anError.getErrorBody());
                        Toast.makeText(TaskActivity.this, "AnError: " + anError, Toast.LENGTH_SHORT).show();
                        progress.dismiss();
                    }
                });
    }


}
