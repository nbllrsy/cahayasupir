package id.ac.astra.polman.cahayasupir.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Admin implements Parcelable {
    private String id_admin;
    private String nama;
    private String nomor_kontak;
    private String email;
    private String foto;
    private String password;
    private String auth_token;

    public Admin() {
    }

    public Admin(String id_admin, String nama, String nomor_kontak, String email, String foto, String password, String auth_token) {
        this.id_admin = id_admin;
        this.nama = nama;
        this.nomor_kontak = nomor_kontak;
        this.email = email;
        this.foto = foto;
        this.password = password;
        this.auth_token = auth_token;
    }

    protected Admin(Parcel in) {
        id_admin = in.readString();
        nama = in.readString();
        nomor_kontak = in.readString();
        email = in.readString();
        foto = in.readString();
        password = in.readString();
        auth_token = in.readString();
    }

    public static final Creator<Admin> CREATOR = new Creator<Admin>() {
        @Override
        public Admin createFromParcel(Parcel in) {
            return new Admin(in);
        }

        @Override
        public Admin[] newArray(int size) {
            return new Admin[size];
        }
    };

    public String getId_admin() {
        return id_admin;
    }

    public void setId_admin(String id_admin) {
        this.id_admin = id_admin;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomor_kontak() {
        return nomor_kontak;
    }

    public void setNomor_kontak(String nomor_kontak) {
        this.nomor_kontak = nomor_kontak;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id_admin);
        dest.writeString(nama);
        dest.writeString(nomor_kontak);
        dest.writeString(email);
        dest.writeString(foto);
        dest.writeString(password);
        dest.writeString(auth_token);
    }
}
