package id.ac.astra.polman.cahayasupir;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class SplashActivity extends AppCompatActivity {
    private final AppCompatActivity activity = SplashActivity.this;
    private TextView mtextWelcome;
    private ImageView mImageViewUnnamed;
    private ImageView mImageViewchy;
    Animation frombottom, fromtop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mtextWelcome = (TextView) findViewById(R.id.textWelcome);
        mImageViewUnnamed=(ImageView) findViewById(R.id.imgUnnamed);
        mImageViewchy=(ImageView) findViewById(R.id.chy);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop= AnimationUtils.loadAnimation(this,R.anim.fromtop);


        mtextWelcome.setAnimation(frombottom);
        mImageViewUnnamed.setAnimation(fromtop);
        mImageViewchy.setAnimation(fromtop);
        final Intent i = new Intent(this,LoginActivity.class);
        Thread timer = new Thread(){
            public void run(){
                try
                {
                    sleep(2000);
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    startActivity(i);
                }
            }
        };
        timer.start();
    }

}
