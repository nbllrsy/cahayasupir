package id.ac.astra.polman.cahayasupir.Fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import id.ac.astra.polman.cahayasupir.EditProfileActivity;
import id.ac.astra.polman.cahayasupir.LoginActivity;
import id.ac.astra.polman.cahayasupir.R;
import id.ac.astra.polman.cahayasupir.Utils.LoginSharedPreference;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    private Button btnLogout;
    private TextView tvNama;
    private TextView tvNomor;
    private TextView tvEmail;
    private ImageView mImageViewEdit;
    private LoginSharedPreference loginSharedPreference;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        loginSharedPreference = new LoginSharedPreference(getActivity());

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        btnLogout = view.findViewById(R.id.btnLogout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });

        tvNama = view.findViewById(R.id.nama_supir);
        tvNomor = view.findViewById(R.id.nomor_kontak);
        tvEmail = view.findViewById(R.id.email);
        mImageViewEdit = (ImageView) view.findViewById(R.id.edit);
        mImageViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profilForm = new Intent(getActivity(), EditProfileActivity.class);
                startActivity(profilForm);
            }
        });

        tvNama.setText(new LoginSharedPreference(getContext()).getSupir().getNama_supir());
        tvNomor.setText(new LoginSharedPreference(getContext()).getSupir().getNomor_kontak());
        tvEmail.setText(new LoginSharedPreference(getContext()).getSupir().getEmail());
        return view;
    }


    private void logout() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity())
                .setTitle("Keluar")
                .setMessage("Anda yakin ingin keluar?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        intentToLogin();
                        loginSharedPreference.setSupir(null);
                        loginSharedPreference.setHasLogin(false);
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        dialog.show();
    }

    private void intentToLogin() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }
}
