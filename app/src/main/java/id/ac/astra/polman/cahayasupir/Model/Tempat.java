package id.ac.astra.polman.cahayasupir.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Tempat implements Parcelable {
    private String id_tempat;
    private String nama_tempat;
    private String jenis;

    public Tempat() {
    }

    public Tempat(String id_tempat, String nama_tempat, String jenis) {
        this.id_tempat = id_tempat;
        this.nama_tempat = nama_tempat;
        this.jenis = jenis;
    }

    protected Tempat(Parcel in) {
        id_tempat = in.readString();
        nama_tempat = in.readString();
        jenis = in.readString();
    }

    public static final Creator<Tempat> CREATOR = new Creator<Tempat>() {
        @Override
        public Tempat createFromParcel(Parcel in) {
            return new Tempat(in);
        }

        @Override
        public Tempat[] newArray(int size) {
            return new Tempat[size];
        }
    };

    public String getId_tempat() {
        return id_tempat;
    }

    public void setId_tempat(String id_tempat) {
        this.id_tempat = id_tempat;
    }

    public String getNama_tempat() {
        return nama_tempat;
    }

    public void setNama_tempat(String nama_tempat) {
        this.nama_tempat = nama_tempat;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id_tempat);
        dest.writeString(nama_tempat);
        dest.writeString(jenis);
    }
}
